**第一步：**把electron的官方例子扒下来，下面简称A，留着待用：

```js
git clone https://github.com/electron/electron-quick-start 
```

**第二步：**进入我们自己的项目（下面简称B），**修改公共路径为相对路径（很多同学都是这步出了问题，导致 npm run build 后出现白屏情况）**：

- **如果你是 vue-cli3 构建的项目：**

执行下列指令，打开vue-cli图形配置界面，选择配置，修改公共路径为 ./ , 保存即可：

```text
vue ui 
```

![img](https://pic2.zhimg.com/80/v2-7f8823532937d935aa325a4001f7d671_720w.jpg)

或者你可以点击上图中右上角的 打开 vue 配置， 或者在项目的根目录中创建 vue.config.js， 设置以下内容：

```js
module.exports = {   
lintOnSave: undefined,   
publicPath: './',   
outputDir: undefined,   
assetsDir: undefined,   
runtimeCompiler: undefined,   
productionSourceMap: undefined,   
parallel: undefined,   
css: undefined 
} 
```

- **如果你是vue-cli2 或者 webpack 创建的项目：**

进入config文件夹下的 index.js ，将其中的 assetsPublicPath 修改为相对路径 ./ ，保存即可：

![img](https://pic2.zhimg.com/80/v2-b6779b759950db90fbbdb8caeccb29c9_720w.jpg)

**第三步：**打包你的项目，我相信这步你已经轻车熟路了~，将打包出来的 dist 文件夹复制到之前下载的A文件夹中

```text
npm run build 
```

**第四步：**进入刚才下载的A项目，删除项目根目录下的 index.html 文件。

**第五步：**在A项目中找到入口文件 main.js ，修改打包的文件路径为我们的index.html：

```js
// main.js 原始内容 
mainWindow.loadFile('index.html') 
// 修改后的内容 
mainWindow.loadFile('./dist/index.html') 
```

**第六步：**在A项目中检查 package.json 的命令，正常情况下，运行下列指令即可进行打包效果预览：

```text
// 国内网络下载electron可能很慢，建议设置
// npm config set registry https://registry.npm.taobao.org/
// npm config set ELECTRON_MIRROR http://npm.taobao.org/mirrors/electron/

npm install 
npm run start 
```

打包成功以后就会出来一个桌面应用，如果一切正常的话你已经可以看到自己的项目了，如果出现白屏的情况，请返回查看第二步~

**第七步：**在A项目中，下载打包需要的依赖 electron-packager

```text
npm install electron-packager --save-dev 
```

**第八步：**在A项目中，进入 package.json ，在 scripts 中添加 packager 指令，如下所示：

```text
"scripts": { 
"start": "electron .", 
"packager": "electron-packager ./ App --platform=win32 --arch=x64 --overwrite"//此处为添加命令
} 
```

如果你想修改最后打包出来的exe文件图标，类似于favicon，或者EXE的名字，可以设置 packager 的指令内容为，icon的路径自己调整下哦，更多配置内容请查阅文档哈：

```text
"packager": "electron-packager ./ YOUR_APP_NAME --platform=win32 --arch=x64 --icon=./dist/favicon.ico --overwrite" 
```

**第九步：**运行命令打包，然后项目中会出现一个 App-win32-x64 的文件夹，这个文件就是打包好的桌面应用，文件夹里有一个 App.exe 文件，App.exe就是这个项目的启动文件：

```text
npm run packager 
```

------

哈哈！大功告成！什么？为什么打包好的不是一个EXE文件，而是一个文件夹？

我只能说太天真！下面我们继续封装EXE安装包

**将桌面应用文件夹封装成EXE安装包**

