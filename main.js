const {
  app,
  BrowserWindow
} = require('electron')
const path = require('path')

const appIcon = './icon/favicon.png'

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 1550,
    height: 800,
    // frame: false,//无标题title 模式
    // maximizable: false,// 禁用最大化
    fullscreenable: false, // 初始化全屏
    autoHideMenuBar: true, // 自动隐藏菜单栏
    resizable: false,// 宽高自动伸缩
    title: '软件音乐',// 标题 
    icon: appIcon,
    backgroundColor: '#ffffff',
    show: false, // 先隐藏窗体
    webSecurity: false, //禁用同源策略
    plugins: true, //是否支持插件
    sandbox: true, //沙盒选项,这个很重要
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })
  // mainWindow.loadURL('https://limestart.cn/')
  mainWindow.loadFile('./dist/index.html')


  // // 初始化后再显示窗体 解决 启动出现暂时的白屏
  mainWindow.on('ready-to-show', function () {
    mainWindow.show()
  })
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})